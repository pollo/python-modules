============================
 Debian Python Team - Policy
============================

:Author: Gustavo Franco <stratus@debian.org>, Raphaël Hertzog <hertzog@debian.org>, Barry Warsaw <barry@debian.org>
:License: GNU GPL v2 or later

:Introduction:
  The Debian Python Team (DPT) aims to improve the Python packages situation in
  Debian, by packaging available modules and applications that may be useful
  and providing a central location for packages maintained by a team, hence
  improving responsiveness, integration, and standardization.

  The DPT is hosted at salsa.debian.org, the Debian GitLab installation. We
  currently have a Git repository and a mailing list whose email address can
  be used in the ``Maintainer`` field on co-maintained
  packages.

  For more information send a message to: debian-python@lists.debian.org

.. contents::


Joining the team
================

The team is open to any Python-related package maintainer. To be added to
the team, please send your request to debian-python@lists.debian.org. Include
the following in your request:

* Why you want to join the team: e.g. maintain your current packages within
  the team, help maintain some specific packages, etc.
* Your Salsa login.
* A statement that you have read
  https://salsa.debian.org/python-team/tools/python-modules/blob/master/policy.rst
  and that you accept it.

The team accepts all contributors and is not restricted to Debian developers.
Several Debian developers of the team will gladly sponsor packages of non-DD
who are part of the team. Sponsorship requests can be sent on the main
discussion list or on #debian-python IRC channel (OFTC network).

All team members should of course follow the main discussion list:
debian-python@lists.debian.org


Maintainership
==============

A package maintained within the team should have the name of the team
in the ``Maintainer`` field.

Maintainer: Debian Python Team <team+python@tracker.debian.org>

This enables the team to have an overview of its packages on the DDPO_website_.

Anyone can commit to the Git repository and upload
as needed.

Team members who have broad interest should subscribe to the mailing list
debian-python@lists.debian.org whereas members who are only interested in some
packages should use the Package Tracking System to follow the packages.


Git Procedures
==============

The DPT uses ``git-buildpackage`` to maintain the packaging, which inherently
uses Git for version control.

For patch management, we choose ``gbp pq``. Optionally you can also manage
patches using only ``quilt``.

Git repositories live on Salsa under the url
``git@salsa.debian.org:python-team/packages/<src-pkg-name>.git``. To access any
source package's repository, use ``gbp clone`` on the above url, substituting
*src-pkg-name* for the source package name of the package in question. You can
also access the GitLab web interface at
``https://salsa.debian.org/python-team/packages/<src-pkg-name>``.

DPT requires a pristine-tar branch, and only upstream tarballs can be used to
advance the upstream branch. Complete upstream Git history should be avoided
in the upstream branch.

Packages must use the ``UNRELEASED`` distribution in ``debian/changelog`` if
the current version in the VCS source has not been uploaded yet. Only when a
package's new version is about to be uploaded should the distribution be
changed to ``unstable`` or another suitable distribution.

Deviations from this policy are strongly discouraged. When you must (not want
to) deviate, you MUST include a ``debian/README.source`` file explaining the
rationale for the deviation and the details of working with the package's git
repo.


Tools
-----

We recommend using ``gbp pq`` to manage patches.  To use this tool, use ``gbp
pq import`` to enter a patch branch, edit the files, commit them, and then use
``gbp pq export`` to switch back to the master branch, with the commits turned
into patches.  Use the pseudo header ``Gbp-Pq`` in the commit message to
control what the patch file will be named, e.g.::

    Gbp-Pq: Name fix-the-foo.patch

Use ``gbp buildpackage --git-tag-only --git-sign-tags`` to tag the repository
when you upload a new release of the package.


Branch names
------------

For the most part, we adhere to DEP-14_ branch naming scheme in the Git
repository, specifically:

* ``debian/master`` - The Debianized upstream source directory. In other words,
  this contains both the *full* upstream source and a ``debian/`` packaging
  directory.  The upstream source *should not apply patches* in the checkout.
* ``pristine-tar`` - Contains the standard ``pristine-tar`` deltas.
* ``upstream`` - The un-Debianized upstream source.  This is what you get when
  you unpack the upstream tarball. This naming rule is derived from DEP-14_ for
  compatibility of current repos.

If you have to use other branch names, you **MUST** document the differences
and explain the reason in ``debian/README.source``. This includes, but is not
limited to, multiple upstream branches, security branches, or downstream
distributions packaging branches.

``gbp pq import`` creates branch beginning with ``patch-queue`` for you to edit
the patches.  However, ``patch-queue`` branches are for local use only, and
should not be pushed to remote servers.

More information
----------------

Additional information for working with DPT Git repositories, creating new
repositories, common workflows, and helpful hints, can be found on the
`Debian wiki <https://wiki.debian.org/Python/GitPackaging>`__
page.


Quality Assurance
=================

The goal of the team is to maintain all packages as best as possible.
Thus every member is encouraged to do general QA work on all the
packages: fix bugs, test packages, improve them to use the latest python
packaging tools, etc.

Tests
=====

Packages maintained under the DPT **MUST** be running the upstream test suite,
both during the build process and as an autopkgtest. As such, the use of
`Testsuite: autopkgtest-pkg-pybuild` is highly recommended.

If a technical issue prevents you from running the test suite and you believe
the reason to be valid, you **MUST** document the differences, explain the
reason in ``debian/README.source`` and preferably report this as a bug on the
BTS.

Updating this Policy
====================

Anyone can propose updates to this policy. Updates should be proposed in the
form of a Merge Request. Proposals should also be sent to the main discussion
list to notify team members of its existence: debian-python@lists.debian.org

A policy update should not be merged before at least seven days to give everyone
a chance to discuss it.

To be merged, a policy update needs the approval of at least one team
administrator.


Code of Conduct
===============

The Debian Python team members agree to the
`Debian Code of Conduct <https://www.debian.org/code_of_conduct>`__
and strive to create a kind and inviting atmosphere among team members.
We are welcoming newcomers and will be open to questions to get them
starting.


License
=======

Copyright (c) 2005-2020 Debian Python Team. All rights reserved.
This document is free software; you may redistribute it and/or modify
it under the same terms as GNU GPL v2 or later.

.. _DDPO_website: https://qa.debian.org/developer.php?login=team%2Bpython%40tracker.debian.org
.. _DEP-14: https://dep-team.pages.debian.net/deps/dep14/
